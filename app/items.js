const express = require('express');
const multer = require('multer');
const mySqlDb = require("../mySqlDb");
const config = require('../config');
const {nanoid} = require("nanoid");
const path = require('path');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});
const router = express.Router();

router.get('/', async (req, res) => {
  const [items] = await mySqlDb.getConnection().query('SELECT id, category_id, location_id,title FROM ??', ['accounting_subjects']);
  res.send(items);
})

router.get('/:id', async (req, res) => {
  const [item] = await mySqlDb.getConnection().query(
    `SELECT * FROM ?? where id = ?`,
    ['accounting_subjects', req.params.id])
  if (!item) {
    return res.status(404).send({error: 'Data not found'});
  }

  res.send(item[0]);
})


router.post('/', upload.single('image'), async (req, res) => {
  if (!req.body.title || !req.body.category_id || !req.body.location_id) {
    return res.status(400).send({error: 'Data not valid'});
  }

  const item = {
    category_id: req.body.category_id,
    location_id: req.body.location_id,
    title: req.body.title,
    description: req.body.description,
  };

  if (req.file) {
    item.image = req.file.filename;
  }

  const [newItem] = await mySqlDb.getConnection().query(
    'INSERT INTO ?? (category_id, location_id, title, description, image) values (?, ?, ?, ?, ?)',
    ['accounting_subjects', item.category_id, item.location_id, item.title, item.description, item.image]
  );

  res.send({
    ...item,
    id: newItem.insertId
  });
});


router.post('/', (req, res) => {
  res.send('post');
});

module.exports = router; // export default router;