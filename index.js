const express = require('express');
const cors = require('cors');
const items = require('./app/items');
const locations = require('./app/locations');
const categories = require('./app/categories');
const mySqlDb = require('./mySqlDb');

const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const port = 8000;

app.use('/items', items);
app.use('/locations', locations);
app.use('/categories', categories);

mySqlDb.connect().catch(e => console.log(e));
app.listen(port, () => {
  console.log(`Server started on ${port} port!`);
});
