drop database if exists inventory;
create database if not exists inventory;
use inventory;

create table if not exists categories (
    id int not null auto_increment primary key,
    title varchar(255) not null,
    description text null
);

create table if not exists locations(
    id int not null auto_increment primary key,
    title varchar(255) not null,
    description text null
);

create table if not exists accounting_subjects(
    id int not null auto_increment primary key,
    category_id int not null,
    location_id int not null,
    title varchar(255) not null,
    description text null,
    image varchar(255) null,
    constraint inventory_category_id_fk
    foreign key(category_id)
    references categories(id)
    on update cascade
    on delete restrict                               ,
    constraint inventory_location_id_fk
    foreign key(location_id)
    references locations(id)
    on update cascade
    on delete restrict
);

insert into categories (title, description)
values ('Household appliances', 'a device or piece of equipment designed to perform a specific task'),
       ('Furniture', 'the movable articles that are used to make a room or building suitable for living'),
       ('Electronic devices', 'Electronic appliances and services related to the personal computer, including the PC (desktop or laptop)');

insert into locations (title, description)
values ('Office #1', null),
       ('Kitchen', null),
       ('IT room', null);

insert into accounting_subjects (category_id, location_id, title, description, image)
values (2, 1, 'Red couch', null, null),
       (3, 3, 'HP ProBook 450', null,null),
       (1, 2, 'Refrigerator LG 201', null,null);

select accounting_subjects.id, accounting_subjects.title, categories.title, locations.title from accounting_subjects
inner join categories on accounting_subjects.category_id = categories.id
inner join locations on accounting_subjects.location_id = locations.id;

